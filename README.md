# IdleOn Crafter

| IdleOn Crafter   | 0.1.0.0                           |
| ---------------- | --------------------------------- |
| Maintainer       | Allele Dev (allele.dev@gmail.com) |
| Copyright        | Allele Dev, 2021                  |
| License          | GPL-3                             |

## Overview

Simple Haskell library for calculating crafting costs for [IdleOn](https://www.legendsofidleon.com) recipes.

## Examples

* Craft 1 item:

```haskell
> Import Idleon
> craft GoldHelmet 1 recipes
[(Fly,2000),(CopperBar,120),(BeanSlices,200),(GoldBar,250)]
```

* Craft many items:

```haskell
> craftMany recipes [(GoldHelmet, 2), (GoldPlatebody, 2)]
[(CopperBar,690),(GoldBar,1200),(Fly,4000),(TrustyNails,1200),(BeanSlices,400),(PocketSand,3000)]
```

## Licensing

This project is distributed under the GPL-3 license. See the included
[LICENSE](./LICENSE) file for more details.

Project logo is an overlay of the official Legends of Idleon logo potion and the Haskell logo.
