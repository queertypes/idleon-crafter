{-# OPTIONS_GHC -Wall #-}
module Idleon (
  Item(..),
  Recipe(..),
  craft,
  craftMany,
  simplify,
  recipes
) where

import Data.List (foldl')
import Data.IntMap.Strict (IntMap)
import Data.Word
import qualified Data.IntMap as IntMap

data Item =
  -- materials
  --- mining
  CopperOre
  | CopperBar
  | IronOre
  | IronBar
  | GoldOre
  | GoldBar
  | PlatinumOre
  | PlatinumBar
  | DementiaOre
  | DementiaBar

  --- choppin
  | OakLogs
  | BleachLogs
  | JungleLogs
  | ForestFibres
  | PottyRolls
  | GrassLeaf
  | Tropilogs
  | VeinyLogs
  | TwistyLeaf
  | TundraLogs
  | WispyLumber
  | ArcticLeaf
  | AlienHiveChunk

  --- fishing
  | Goldfish
  | HermitCan
  | Bloach
  | Jellyfish

  --- catching
  | Fly
  | ButterFly

  --- smithing
  | Thread
  | TrustyNails
  | BoringBrick
  | ChainLink
  | LeatherHide
  | PinionSpur
  | LugiBracket

  --- alchemy
  | DistilledWater

  --- critters/trapping
  | PoisonFroge
  | Froge

  --- worship
  | ForestSoul

  --- construction
  | RedoxSalts

  --- mobbing
  ---- blunderhills
  | SporeCap
  | FrogLeg
  | BeanSlices
  | RedSporeCap
  | SlimeSludge
  | SnakeSkin
  | CarrotCube
  | GoblinEar
  | Plank
  | BullfrogHorn
  | AmarokSlab
  | Stick
  | Acorn
  | CornKernels
  | ChaoticAmarokSlab

  ---- yumyum desert
  | PocketSand
  | GlassShard
  | MegalodonTooth
  | CrabbyCakey

  --- food
  | Nomwich
  | HotDog
  | Pizza
  | SmallManaPotion
  | SmallLifePotion
  | MeatPie

  --- tokens
  | EasyBlunderhillsNPCToken
  | MedBlunderhillsNPCToken
  | HardBlunderhillsNPCToken
  | WoodsmanToken
  | GlumleeToken
  | StiltzchoToken
  | FunguyToken
  | KrunkToken
  | TikiChiefToken
  | DogBoneToken
  | PapuaPiggeaToken
  | PicnicToken
  | TPPeteToken
  | SproutinaldToken
  | DazeyToken

  --- misc
  | CrimsonString
  | CueTape
  | CrudeOil
  | SporeTee
  | DankPaypayChest
  | CarrotLauncher
  | BoltCutters
  | GoldenPlop
  | WoodularCircle
  | GoldenDubloon

  -- equipment
  --- tab I
  | BoxingGloves
  | WoodenSpear
  | WoodenBow
  | GnarledWand
  | FarmerBrim
  | OrangeTee
  | TornJeans
  | FlipFlops
  | CopperPickaxe
  | CrampedMiningPouch
  | CopperChopper
  | CrampedChoppinPouch
  | LeatherCap
  | TheStingers
  | CrampedFoodPouch
  | CrampedMaterialPouch
  | CopperHelmet
  | CopperPlatebody
  | CopperPlatelegs
  | CopperBoots
  | MilitiaHelmet
  | ThiefHood
  | TopHat
  | PartyHat
  | SteelAxe
  | BirchLongbow
  | Quarterstaff
  | CopperBand
  | IronPickaxe
  | SmallMiningPouch
  | IronHatchet
  | SmallChoppinPouch
  | IronHelmet
  | IronPlatebody
  | IronPlatelegs
  | AnvilTab2
  | BlueTee
  | SleekShank
  | IronBoots
  | SteelBand
  | PurpleTupacband
  | IcingIronbite
  | SaucyLogfries
  | GooGaloshes
  | FurShirt
  | HideShirt
  | EcoFriendlyOil
  | DoobleGoopi
  | BleachedDesignerWodePatchPants
  | DirtyCoalMinerBaggySootPants
  | Peanut
  | GoldenPeanut
  | Blunderbag
  | PurpleTee
  | GreenTee
  | BlackTee
  | BlunderhillsNPCCompletionToken
  | BlunderSkillsCompletionToken
  | BlunderhillsMiscCompletionToken
  | AmarokHelmet
  | AmarokBodyplate
  | AmarokHinds
  | AmarokPaws
  | FishingOveralls
  | BanditoPantaloon
  | ChaoticAmarokPendant
  | CopperFishRod
  | CrampedFishPouch
  | CopperNettedNet
  | CrampedBugPouch
  | IronFishingRod
  | SmallFishPouch
  | ReinforcedNet
  | SmallBugPouch
  | SilkskinTraps
  | SmallCritterPouch
  | CeramicSkull
  | SmallSoulPouch

  --- tab II
  | GoldHelmet
  | GoldPlatebody
  | GoldPlatelegs
  | GoldBoots
  | BandageWraps
  | RoyalBayonet
  | SpikedMenace
  | Starlight
  | GoldPickaxe
  | AverageMiningPouch
  | GoldenAxe
  | AverageChoppinPouch
  | GoldFishingRod
  | AverageFishPouch
  | GildedNet
  | AverageBugPouch
  | DefendersDignity
  | StrungBludgeon
  | AverageFoodPouch
  | PlatinumHelmet
  | PlatinumPlatebody
  | PlatinumShins
  | PlatinumBoots
  | EnforcedSlasher
  | PharoahBow
  | CrowsNest
  | PlatinumPickaxe
  | SizableMiningPouch
  | PlatHatchet
  | SizableChoppinPouch
  | PlatFishingRod
  | SizableFishPouch
  | Platinet
  | SizableBugPouch
  | SizableFoodPouch
  | SizableMaterialsPouch
  | AnvilTab3
  | EmptyBox
  | GoogleyEyes
  | FMJBullet
  | VikingCap
  | SleekCoif
  | WitchHat
  | StuddedHide
  | FeralLeathering
  | FurledRobes
  | CavernTrekkers
  | AnglerBoots
  | LoggerHeels
  | BanditoBoots
  | SlurpinHerm
  | ButteredToastedButter
  | DootjatEye
  | SandySatchel
  | EfauntHelmet
  | EfauntRibcage
  | EfauntsBrokenAnkles
  | EfauntTrunculus
  | WoodenTraps
  | SizableCritterPouch
  | HornedSkull
  | SizableSoulPouch
  | YumyumDesertNPCCompletionToken
  | YumyumSkillsCompletionToken
  | YumyumMiscCompletionToken
  | EasyYumyumDezNPCToken
  | MedYumyumDezNPCToken
  | HardYumyumDezNPCToken
  deriving (Ord, Eq, Enum, Show)

data Recipe = Recipe Item [(Item, Word32)]
type Recipes = IntMap [(Item, Word32)]


craft :: Item -> Word32 -> Recipes -> [(Item, Word32)]
craft i' amount' rs' = go i' amount' rs' []
  where
    go :: Item -> Word32 -> Recipes -> [(Item, Word32)] -> [(Item, Word32)]
    go i amount rs acc =
      let r = IntMap.lookup (fromEnum i) rs in
        case r of
          Nothing -> acc
          (Just []) -> [(i, amount)]
          (Just mats) -> foldMap (\(m,q) -> go m (q*amount) rs acc) mats <> acc

craftMany :: Recipes -> [(Item, Word32)] -> [(Item, Word32)]
craftMany rs = simplify . foldl' (\acc (i,q) -> craft i q rs <> acc) []

simplify :: [(Item, Word32)] -> [(Item, Word32)]
simplify =
  fmap (\(i,q) -> (toEnum i, q))
  . IntMap.toList
  . IntMap.fromListWith (+)
  . fmap (\(m,q) -> (fromEnum m, q))

--------------------------------------------------------------------------------
-- data
--------------------------------------------------------------------------------
recipes :: Recipes
recipes = IntMap.fromList . fmap convert $ [
    (CopperOre, []),
    (CopperBar, []),
    (IronOre, []),
    (IronBar, []),
    (GoldOre, []),
    (GoldBar, []),
    (PlatinumOre, []),
    (PlatinumBar, []),
    (DementiaOre, []),
    (DementiaBar, []),
    (OakLogs, []),
    (BleachLogs, []),
    (JungleLogs, []),
    (ForestFibres, []),
    (PottyRolls, []),
    (GrassLeaf, []),
    (Tropilogs, []),
    (VeinyLogs, []),
    (TwistyLeaf, []),
    (TundraLogs, []),
    (WispyLumber, []),
    (ArcticLeaf, []),
    (AlienHiveChunk, []),
    (Goldfish, []),
    (HermitCan, []),
    (Bloach, []),
    (Jellyfish, []),
    (Fly, []),
    (ButterFly, []),
    (Thread, []),
    (TrustyNails, []),
    (BoringBrick, []),
    (ChainLink, []),
    (LeatherHide, []),
    (PinionSpur, []),
    (LugiBracket, []),
    (DistilledWater, []),
    (PoisonFroge, []),
    (Froge, []),
    (ForestSoul, []),
    (RedoxSalts, []),
    (SporeCap, []),
    (FrogLeg, []),
    (BeanSlices, []),
    (RedSporeCap, []),
    (SlimeSludge, []),
    (SnakeSkin, []),
    (CarrotCube, []),
    (GoblinEar, []),
    (Plank, []),
    (BullfrogHorn, []),
    (AmarokSlab, []),
    (Stick, []),
    (Acorn, []),
    (CornKernels, []),
    (ChaoticAmarokSlab, []),
    (PocketSand, []),
    (MegalodonTooth, []),
    (CrabbyCakey, []),
    (Nomwich, []),
    (HotDog, []),
    (Pizza, []),
    (EasyBlunderhillsNPCToken, []),
    (MedBlunderhillsNPCToken, []),
    (HardBlunderhillsNPCToken, []),
    (WoodsmanToken, []),
    (GlumleeToken, []),
    (StiltzchoToken, []),
    (FunguyToken, []),
    (KrunkToken, []),
    (TikiChiefToken, []),
    (DogBoneToken, []),
    (PapuaPiggeaToken, []),
    (PicnicToken, []),
    (TPPeteToken, []),
    (SproutinaldToken, []),
    (DazeyToken, []),
    (CrimsonString, []),
    (CueTape, []),
    (CrudeOil, []),
    (SporeTee, []),
    (DankPaypayChest, []),
    (CarrotLauncher, []),
    (BoltCutters, []),
    (GoldenPlop, []),
    (WoodularCircle, []),
    (GoldenDubloon, []),
    (GlassShard, []),
    (MeatPie, []),
    -- tab I
    (BoxingGloves, [(CrimsonString, 1), (CueTape, 5)]),
    (WoodenSpear, [(SporeCap, 25), (CopperBar, 10)]),
    (WoodenBow, [(SporeCap, 25), (Thread, 30)]),
    (GnarledWand, [(SporeCap, 25), (OakLogs, 30)]),
    (FarmerBrim, [(SporeCap, 5), (CopperOre, 15)]),
    (OrangeTee, [(SporeCap, 10), (OakLogs, 15)]),
    (TornJeans, [(FrogLeg, 20), (CopperBar, 8), (Thread, 15)]),
    (FlipFlops, [(BeanSlices, 30), (CopperBar, 20), (GrassLeaf, 20)]),
    (CopperPickaxe, [(CopperBar, 13), (FrogLeg, 30)]),
    (CrampedMiningPouch, [(CopperOre, 120)]),
    (CopperChopper, [(CopperBar, 13), (OakLogs, 30), (Thread, 15)]),
    (CrampedChoppinPouch, [(OakLogs, 150), (TrustyNails, 30)]),
    (LeatherCap, [(FrogLeg, 25), (OakLogs, 45), (TrustyNails, 20)]),
    (TheStingers, [(CopperBar, 20), (OakLogs, 75), (TrustyNails, 20)]),
    (CrampedFoodPouch, [(Nomwich, 75), (HotDog, 25), (OakLogs, 60), (CopperBar, 15)]),
    (CrampedMaterialPouch, [(TrustyNails, 80), (FrogLeg, 40), (BeanSlices, 30)]),
    (CopperHelmet, [(CopperBar, 30), (BeanSlices, 50)]),
    (CopperPlatebody, [(CopperBar, 45), (TrustyNails, 120)]),
    (CopperPlatelegs, [(CopperBar, 60), (BleachLogs, 150)]),
    (CopperBoots, [(CopperBar, 125)]),
    (MilitiaHelmet, [(IronBar, 25), (CopperOre, 150), (CopperHelmet, 2)]),
    (ThiefHood, [(IronBar, 25), (Thread, 150), (CopperHelmet, 2)]),
    (TopHat, [(IronBar, 25), (OakLogs, 150), (CopperHelmet, 2)]),
    (PartyHat, [(IronBar, 200), (CopperHelmet, 4)]),
    (SteelAxe, [(SlimeSludge, 125), (BoringBrick, 125)]),
    (BirchLongbow, [(SlimeSludge, 125), (BleachLogs, 250)]),
    (Quarterstaff, [(SlimeSludge, 125), (CopperBar, 100)]),
    (CopperBand, [(CopperBar, 300)]),
    (IronPickaxe, [(IronBar, 100), (CarrotCube, 200)]),
    (SmallMiningPouch, [(IronOre, 700)]),
    (IronHatchet, [(JungleLogs, 250), (BoringBrick, 125)]),
    (SmallChoppinPouch, [(BleachLogs, 800)]),
    (IronHelmet, [(IronBar, 125), (GoblinEar, 300)]),
    (IronPlatebody, [(IronBar, 150), (JungleLogs, 600)]),
    (IronPlatelegs, [(IronBar, 170), (BoringBrick, 225)]),
    (AnvilTab2, [(IronBar, 250), (ChainLink, 200), (ForestFibres, 400), (BullfrogHorn, 250)]),
    (BlueTee, [(SporeCap, 25), (OakLogs, 50), (OrangeTee, 2)]),
    (SleekShank, [(Thread, 300), (WoodenSpear, 3), (WoodenBow, 3), (GnarledWand, 3)]),
    (IronBoots, [(TrustyNails, 500), (IronBar, 125), (CopperBoots, 2)]),
    (SteelBand, [(IronBar, 600)]),
    (PurpleTupacband, [(BleachLogs, 100), (IronOre, 50), (CrudeOil, 60)]),
    (IcingIronbite, [(IronOre, 5), (SmallManaPotion, 2)]),
    (SaucyLogfries, [(JungleLogs, 6), (SmallLifePotion, 3)]),
    (GooGaloshes, [(SlimeSludge, 600), (JungleLogs, 1000), (CopperBoots, 2)]),
    (FurShirt, [(GoblinEar, 500), (IronBar, 500)]),
    (HideShirt, [(BleachLogs, 325), (CopperPlatebody, 2), (SteelAxe, 1)]),
    (EcoFriendlyOil, [(CrudeOil, 1), (GrassLeaf, 1)]),
    (DoobleGoopi, [(SlimeSludge, 1000), (CopperBand, 1), (SteelBand, 1)]),
    (BleachedDesignerWodePatchPants, [(TornJeans, 10), (Plank, 275), (BleachLogs, 700)]),
    (DirtyCoalMinerBaggySootPants, [(TornJeans, 8), (IronOre, 200)]),
    (Peanut, [(HotDog, 2), (CopperOre, 1), (BleachLogs, 1)]),
    (Blunderbag, [(SporeTee, 2), (DankPaypayChest, 1), (SporeCap, 169)]),
    (PurpleTee, [(RedSporeCap, 40), (BleachLogs, 100), (OrangeTee, 3)]),
    (GreenTee, [(Thread, 150), (SporeTee, 2), (OrangeTee, 3)]),
    (BlackTee, [(OrangeTee, 5), (BlueTee, 3), (PurpleTee, 3), (GreenTee, 3)]),
    (BlunderhillsNPCCompletionToken, [(EasyBlunderhillsNPCToken, 1)
                                     , (MedBlunderhillsNPCToken, 1)
                                     , (HardBlunderhillsNPCToken, 1)]),
    (BlunderSkillsCompletionToken, [(PlatinumBar, 1000), (VeinyLogs, 2000), (AmarokHelmet, 1)]),
    (BlunderhillsMiscCompletionToken, [ (GoldenPeanut, 30), (GoldenPlop, 12)
                                      , (BoltCutters, 1), (CarrotLauncher, 2)
                                      ]),
    (EasyBlunderhillsNPCToken, [ (WoodsmanToken, 1), (GlumleeToken, 1)
                               , (StiltzchoToken, 1), (FunguyToken, 1)
                               ]),
    (MedBlunderhillsNPCToken, [ (KrunkToken, 1), (TikiChiefToken, 1)
                              , (DogBoneToken, 1), (PapuaPiggeaToken, 1)
                              ]),
    (HardBlunderhillsNPCToken, [ (PicnicToken, 1), (TPPeteToken, 1)
                               , (SproutinaldToken, 1), (DazeyToken, 1)
                               ]),
    (AmarokHelmet, [(AmarokSlab, 20), (ForestFibres, 2500), (WoodularCircle, 8)]),
    (AmarokBodyplate, [(AmarokSlab, 10), (IronBar, 400), (CornKernels, 900)]),
    (AmarokHinds, [(AmarokSlab, 6), (ChainLink, 400), (BullfrogHorn, 700)]),
    (AmarokPaws, [(AmarokSlab, 5), (GrassLeaf, 125), (IronBoots, 1)]),
    (FishingOveralls, [(BleachedDesignerWodePatchPants, 3), (Goldfish, 500)]),
    (BanditoPantaloon, [(DirtyCoalMinerBaggySootPants, 3), (Fly, 500)]),
    (ChaoticAmarokPendant, [(ChaoticAmarokSlab, 4), (AmarokSlab, 10), (DistilledWater, 25)]),
    (CopperFishRod, [(CopperBar, 200), (Goldfish, 400)]),
    (CrampedFishPouch, [(Goldfish, 180)]),
    (CopperNettedNet, [(JungleLogs, 500), (Fly, 400)]),
    (CrampedBugPouch, [(Fly, 180)]),
    (IronFishingRod, [(IronBar, 300), (HermitCan, 850), (DistilledWater, 3)]),
    (SmallFishPouch, [(HermitCan, 425), (DistilledWater, 2)]),
    (ReinforcedNet, [(ForestFibres, 800), (ButterFly, 850), (DistilledWater, 3)]),
    (SmallBugPouch, [(ButterFly, 425), (DistilledWater, 2)]),
    (SilkskinTraps, [(PoisonFroge, 1), (RedoxSalts, 5)]),
    (SmallCritterPouch, [(Froge, 200), (RedoxSalts, 3)]),
    (CeramicSkull, [(ForestSoul, 250), (RedoxSalts, 5)]),
    (SmallSoulPouch, [(ForestSoul, 200), (RedoxSalts, 3)]),
    (GoldenPeanut, [(Peanut, 100), (GoldBar, 50)]),
    -- tab II
    (GoldHelmet, [(Fly, 2000), (CopperHelmet, 4), (GoldBar, 250)]),
    (GoldPlatebody, [(PocketSand, 1500), (CopperPlatebody, 5), (GoldBar, 350)]),
    (GoldPlatelegs, [(Goldfish, 500), (CopperPlatelegs, 6), (GoldBar, 475)]),
    (GoldBoots, [(GoldBar, 600), (IronBoots, 2), (DistilledWater, 4)]),
    (BandageWraps, [(CueTape, 5000), (TheStingers, 5), (PottyRolls, 2000)]),
    (RoyalBayonet, [(ForestFibres, 1250), (SteelAxe, 3), (DistilledWater, 8)]),
    (SpikedMenace, [(GoldBar, 250), (BirchLongbow, 3), (DistilledWater, 8)]),
    (Starlight, [(ChainLink, 1000), (Quarterstaff, 3), (DistilledWater, 8)]),
    (GoldPickaxe, [(MegalodonTooth, 2500), (GoldBar, 800), (IronPickaxe, 3)]),
    (AverageMiningPouch, [(CrampedMiningPouch, 5), (SmallMiningPouch, 3), (GoldOre, 1400)]),
    (GoldenAxe, [(GoldenDubloon, 5), (CrabbyCakey, 2000), (IronHatchet, 3)]),
    (AverageChoppinPouch, [ (CrampedChoppinPouch, 5), (SmallChoppinPouch, 3)
                          , (ForestFibres, 1800)]),
    (GoldFishingRod, [(HermitCan, 2500), (LeatherHide, 1000), (IronFishingRod, 3)]),
    (AverageFishPouch, [(CrampedFishPouch, 5), (SmallFishPouch, 3), (HermitCan, 1500)]),
    (GildedNet, [(GoldenDubloon, 5), (ButterFly, 2500), (ReinforcedNet, 3)]),
    (AverageBugPouch, [(CrampedBugPouch, 5), (SmallBugPouch, 3), (ButterFly, 1600)]),
    (DefendersDignity, [(Goldfish, 10000), (GoldenPlop, 4), (DistilledWater, 15)]),
    (StrungBludgeon, [(BullfrogHorn, 9000), (GlassShard, 10), (DistilledWater, 20)]),
    (AverageFoodPouch, [ (CrampedFoodPouch, 7), (MeatPie, 250)
                       , (IcingIronbite, 600), (SaucyLogfries, 600)]),
    (PlatinumHelmet, []),
    (PlatinumPlatebody, []),
    (PlatinumShins, []),
    (PlatinumBoots, []),
    (EnforcedSlasher, []),
    (PharoahBow, []),
    (CrowsNest, []),
    (PlatinumPickaxe, []),
    (SizableMiningPouch, []),
    (PlatHatchet, []),
    (SizableChoppinPouch, []),
    (PlatFishingRod, []),
    (SizableFishPouch, []),
    (Platinet, []),
    (SizableBugPouch, []),
    (SizableFoodPouch, []),
    (SizableMaterialsPouch, []),
    (AnvilTab3, []),
    (EmptyBox, []),
    (GoogleyEyes, []),
    (FMJBullet, []),
    (VikingCap, []),
    (SleekCoif, []),
    (WitchHat, []),
    (StuddedHide, []),
    (FeralLeathering, []),
    (FurledRobes, []),
    (CavernTrekkers, []),
    (AnglerBoots, []),
    (LoggerHeels, []),
    (BanditoBoots, []),
    (SlurpinHerm, []),
    (ButteredToastedButter, []),
    (DootjatEye, []),
    (SandySatchel, []),
    (EfauntHelmet, []),
    (EfauntRibcage, []),
    (EfauntsBrokenAnkles, []),
    (EfauntTrunculus, []),
    (WoodenTraps, []),
    (SizableCritterPouch, []),
    (HornedSkull, []),
    (SizableSoulPouch, []),
    (YumyumDesertNPCCompletionToken, []),
    (YumyumSkillsCompletionToken, []),
    (YumyumMiscCompletionToken, []),
    (EasyYumyumDezNPCToken, []),
    (MedYumyumDezNPCToken, []),
    (HardYumyumDezNPCToken, [])
  ]
  where convert (x, xs) = (fromEnum x, xs)
